# Redis - základy

### Ověření funkčnosti redisu
První věc, kterou musíme udělat, abychom zjistili zda Redis funguje správně je odeslání příkazu PING
```
> ping
pong
```

### Vkládání a získávání dat z databáze
Pro vložení dat slouží příkaz **set**
```
> set klic hodnota
OK
```
Pro získání dat slouží příkaz **get**
```
> get klic
"hodnota"
```

### Základní pravidla pro klíče
- Klíče by neměli být příliš dlouhé, kvůli úspoře RAM
- Klíče by měli mít vypovídající hodnotu (u1000flw vs user:1000:followers")
- Doporučené schéma tvorby klíčů je objekt:id

### String
Jedná se o nejednodušší datový typ v Redisu. Maximální velikost stringu může být 512MB
```
> set counter 10
OK
> get counter
"10"
> incr counter
(integer) "11"
> decr counter
(integer) "10"
```
Můžeme vkládat a získávat data hromadně (mělo by to být ještě rychlejší)
```
> mset a 10 b 20 c 30
OK
> mget a b c
1) "10"
2) "20"
3) "30"
```
Mazaní klíče a zjišťování existence klíče
```
> set klic ahoj
OK
> exists klic
(integer) 1
> del klic
(integer) 1
> exists klic
(integer) 0
```
### Nastavení platnosti klíče
Jedna z nejdůležitějších vlastností redisu. Vypršení platnosti klíče umožňuje nastavit časový limit pro klíč, známý také jako „time to live“ nebo „TTL“. Když TTL uplyne, klíč je automaticky zničen.
```
> set klic David
OK
> expire klic 10
(integer) 1
> get klic
"David"
> get klic
(nil)
> set klic ex 5
OK
> ttl klic
(integer) 9
```

### Seznamy
Seznamy tvořené v redisu jsou implementované jako spojové seznamy. Každý prvek seznamu teda obsahuje, kromě své hodnoty, odkaz na následující i předchozí prvky seznamu.
Důsledkem je že přidávání na nové položky na začátek i konec seznamu se prování v konstantním čase. Neefektivní je naopak vybírání prvku z prostředku seznamu. K tomu pak slouží jiná datová struktura najívající se kolekce.
```
> rpush list A
(integer) 1
> rpush list B
(integer) 2
> lpush list first
(integer) 3
> lrange list 0 -1
1) "first"
2) "A"
3) "B"
> rpush mylist 1 2 3 4 5 "foo bar"
(integer) 9
> lrange mylist 0 -1
1) "first"
2) "A"
3) "B"
4) "1"
5) "2"
6) "3"
7) "4"
8) "5"
9) "foo bar"
> rpush mylist a b c
(integer) 3
> rpop mylist
"c"
> rpop mylist
"b"
> rpop mylist
"a"
```
Nejčastěji se seznamy používají pro ukládání nejnovějších položek. LTRIM nám umožňuje seznam omezit tak, že položky za LTRIM velikostí seznamu jsou vymazány
```
> rpush mylist 1 2 3 4 5
(integer) 5
> ltrim mylist 0 2
OK
> lrange mylist 0 -1
1) "1"
2) "2"
3) "3"
> del mylist
(integer) 1
```

### Hashe
Stejné jako slovníky v pragramujících jazycích
```
> hset user:1000 username antirez birthyear 1977 verified 1
(integer) 3
> hget user:1000 username
"antirez"
> hget user:1000 birthyear
"1977"
> hgetall user:1000
1) "username"
2) "antirez"
3) "birthyear"
4) "1977"
5) "verified"
6) "1"
```

### Sady
Sady jsou neuspořádaná kolekce stringů. Umožňuje operace na množinách jako je průnik, rozdíl nebo sjednocení.
```
> sadd myset 1 2 3
(integer) 3
> smembers myset
1. 3
2. 1
3. 2
> sismember myset 3
(integer) 1
> sismember myset 30
(integer) 0
```

### Seřazené sady
Jedná se o mix sad a hashů. Fungují stejně jako sady jen každá hodnota navíc ještě obsahuje skóre, které určuje pořadí prvků.
```
> zadd hackers 1916 "Claude Shannon"
(integer) 1
> zadd hackers 1969 "Linus Torvalds"
(integer) 1
> zadd hackers 1912 "Alan Turing"
(integer) 1
> zrange hackers 0 -1
1) "Alan Turing"
3) "Claude Shannon"
9) "Linus Torvalds"
```
(-1 po poslední prvek)

### Bitmapy
Bitmapy nejsou skutečným datovým typem, ale sadou bitově orientovaných operací definovaných na typu String.
U prvního příkladu nastavujeme desátý bit na hodnotu 1.
```
> setbit key 10 1
(integer) 1
> getbit key 10
(integer) 1
> getbit key 11
(integer) 0
> setbit key 0 1
(integer) 0
> setbit key 100 1
(integer) 0
> bitcount key
(integer) 2
```

# Děkuji za pozornost !!
